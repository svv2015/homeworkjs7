function filterBy(arr, type) {
    return arr.filter(function (item) {
        return typeof item !== type;
    });
}
let arr = ['hello', 'world', 23, '23', null];
let filteredArr = filterBy(arr, 'string');
console.log(arr);
console.log(filteredArr);